## Hardware Setup

<img src="image1.jpg"/>

<img src="image5.jpg"/>

<img src="image6.jpg"/>

<img src="image7.jpg"/>

## Pre-requisite Cloud and software

* Ngrok software download

    https://ngrok.com/

* Twilio Account

    https://www.twilio.com/

* Gmail Account

    https://www.gmail.com

* Fill all this values with your own config and cloud subscription values. Kindly use .env file for the program to pick it up.

```
SMTP_GMAIL_ACC=
SMTP_GMAIL_PASSWORD=
TWILIO_SSID=
TWILIO_AUTH_TOKEN=
TWILIO_WHATSAPP_NO=
TWILIO_NUMBER=
SMS_TO_NUMBER=
EMAIL_TO=
SIGFOX_SERVER_PORT=3000
MIN_LOW_THRESHOLD=0
MAX_LOW_THRESHOLD=2
MIN_MEDIUM_THRESHOLD=3
MAX_MEDIUM_THRESHOLD=5
MIN_MODERATE_THRESHOLD=6
MAX_MODERATE_THRESHOLD=8
MAX_HIGH_THRESHOLD=9
```

## How to run the backend callback server

```
cd $PROJECT_DIRECTORY
npm i 
node notification-server.js
```

## Launch another terminal to run tcp tunnelling

```
./ngrok http 3000 
```

## Copy & paste the the callback URL from ngrok to the sigfox console as follows.

<img src="image8.jpg"/>

## Notification (SMS, Email and WhatsApp)


<img src="image2.jpg"/>

<img src="image3.jpg"/>

<img src="image4.jpg"/>