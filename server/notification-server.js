'use strict';
require('events').EventEmitter.defaultMaxListeners = 0
require("dotenv").config();
const express = require("express"),
    bodyParser = require("body-parser"),
    structuredMessage = require("./structuredMessage"),
    _ = require("lodash"),
    notification = require('./util/notification'),
    path = require("path");

var app = express();
const sms = new notification.SMS();
const email = new notification.Email();

// init router
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
app.use(bodyParser.json({ limit: "50mb" }));
app.use(express.static(path.join(__dirname, "public")));

var router = express.Router();

app.use("/", router);

const minLowthreshold = process.env.MIN_LOW_THRESHOLD;
const maxLowthreshold = process.env.MAX_LOW_THRESHOLD;
const minMediumthreshold = process.env.MIN_MEDIUM_THRESHOLD;
const maxMediumthreshold = process.env.MAX_MEDIUM_THRESHOLD;
const minModeratethreshold = process.env.MIN_MODERATE_THRESHOLD;
const maxModeratethreshold = process.env.MAX_MODERATE_THRESHOLD;
const maxHighthreshold = process.env.MAX_HIGH_THRESHOLD;

function checkWaterLevelThreshold(result) {
    console.log("Check water level threshold... " + result);
    if (result.rng !== 'undefined') {
        console.log(result.rng);
        let waterLevelValue = parseInt(result.rng);
        console.log(waterLevelValue);
        console.log(parseInt(maxHighthreshold));
        
        console.log(waterLevelValue > parseInt(maxHighthreshold));
        if (waterLevelValue >= parseInt(minLowthreshold) && waterLevelValue <= parseInt(maxLowthreshold)) {
            console.log("NOTIFY >> minLowthreshold >>> " + minLowthreshold);
            console.log("NOTIFY >> maxLowthreshold >>> " + maxLowthreshold);
            sms.send(`Water level is LOW ! Reading at ${
                waterLevelValue
                } cm. `, process.env.SMS_TO_NUMBER);

            email.send(
                process.env.EMAIL_TO,
                `Water level is LOW !`,
                `<p>Water level is LOW ! ${new Date().toLocaleString("en-US", {
                    timeZone: "Asia/Singapore"
                })}. Reading at ${
                waterLevelValue
                } cm.</p>`);
        } else if (waterLevelValue >= parseInt(minMediumthreshold) &&
            waterLevelValue <= parseInt(maxMediumthreshold)) {
            console.log("NOTIFY >> mediumthreshold >>> " + minMediumthreshold);
            console.log("NOTIFY >> mediumthreshold >>> " + maxMediumthreshold);
            sms.send(`Water level is medium ! Reading at ${
                waterLevelValue
                } cm. `, process.env.SMS_TO_NUMBER);

            email.send(
                process.env.EMAIL_TO,
                `Water level is MEDIUM !`,
                `<p>Water level is MEDIUM ! ${new Date().toLocaleString("en-US", {
                    timeZone: "Asia/Singapore"
                })}. Reading at ${
                waterLevelValue
                } cm.</p>`);
        } else if (waterLevelValue >= parseInt(minModeratethreshold) &&
            waterLevelValue <= parseInt(maxModeratethreshold)) {
            console.log("NOTIFY >> minModeratethreshold >>> " + minModeratethreshold);
            console.log("NOTIFY >> maxModeratethreshold >>> " + maxModeratethreshold);
            sms.send(`Water level is moderate ! Reading at ${
                waterLevelValue
                } cm. `, process.env.SMS_TO_NUMBER);

            email.send(
                process.env.EMAIL_TO,
                `Water level is moderate !`,
                `<p>Water level is moderate ! ${new Date().toLocaleString("en-US", {
                    timeZone: "Asia/Singapore"
                })}. Reading at ${
                waterLevelValue
                } cm.</p>`);
        } else if (waterLevelValue >= parseInt(maxHighthreshold)) {
            console.log("NOTIFY >> highthreshold >>> " + maxHighthreshold);
            sms.send(`Water level is HIGH ! Reading at ${
                waterLevelValue
                } cm. `, process.env.SMS_TO_NUMBER);

            email.send(
                process.env.EMAIL_TO,
                `Water level is HIGH !`,
                `<p>Water level is HIGH ! ${new Date().toLocaleString("en-US", {
                    timeZone: "Asia/Singapore"
                })}. Reading at ${
                waterLevelValue
                } cm.</p>`);
        }
    }
}

router.post("/sigfox-callback", (req, res, next) => {
    if (!req.body || !req.body.data)
        res.status(500).json(Object.assign({}, req.body));
    try {
        console.log(req.body);
        const decodedData = structuredMessage.decodeMessage(req.body.data);
        console.log(decodedData);
        const result = Object.assign({}, req.body, decodedData);
        checkWaterLevelThreshold(decodedData);
        res.status(200).json({});
    } catch (error) {
        console.log(error);
        //  In case of error, return the original message.
        res.status(500).json(req.body);
    }
});

router.post("/sigfox-error", (req, res, next) => {
    console.log("/sigfox-error");
    console.log(req.body);
    res.json({});
});

router.post("/sigfox-service", (req, res, next) => {
    console.log("/sigfox-service");
    console.log(req.body);
    res.json({});
});

const PORT_NUMBER = process.env.SIGFOX_SERVER_PORT;

app.listen(PORT_NUMBER, function () {
    console.log(`Sigfox Callback Server is running on port ${PORT_NUMBER}`);
});