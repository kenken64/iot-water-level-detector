// libraries
#include "Ultrasonic.h"
#include "rgb_lcd.h"
#include "SIGFOX.h"

//  IMPORTANT: Check these settings with UnaBiz to use the SIGFOX library correctly.
static const String device = "wld2020sg";  //  Set this to your device name if you're using UnaBiz Emulator.
static const bool useEmulator = false;  //  Set to true if using UnaBiz Emulator.
static const bool echo = true;  //  Set to true if the SIGFOX library should display the executed commands.
static const Country country = COUNTRY_SG;  //  Set this to your country to configure the SIGFOX transmission frequencies.
static UnaShieldV2S transceiver(country, useEmulator, device, echo);  //  Uncomment this for UnaBiz UnaShield V2S / V2S2 Dev Kit

Ultrasonic ultrasonic(8);
rgb_lcd lcd;


void Callback1();

const int colorR = 0;
const int colorG = 0;
const int colorB = 255;


void setup() 
{
    Serial.begin(9600);
    // set up the LCD's number of columns and rows:
    lcd.begin(16, 2);
    lcd.setRGB(colorR, colorG, colorB);
    delay(200);
    //  Check whether the SIGFOX module is functioning.
    if (!transceiver.begin()) stop(F("Unable to init SIGFOX module, may be missing"));  //  Will never return.
  
    //  Send a raw 12-byte message payload to SIGFOX.  In the loop() function we will use the Message class, which sends structured messages.
    transceiver.sendMessage("0102030405060708090a0b0c");
    
    //  Delay 10 seconds before sending next message.
    Serial.println(F("Waiting 10 seconds..."));
    delay(10000);
}

void loop() 
{
      static int successCount = 0, failCount = 0; 
      char mystr[40];
      long RangeInCentimeters = 0;
      float RangeInCentimeters_f = 0;
      lcd.clear();
      RangeInCentimeters = ultrasonic.MeasureInCentimeters(); // two measurements should keep an interval
      sprintf(mystr,"Range(cm): %u",RangeInCentimeters);
      lcd.setCursor(0, 1);
      lcd.print(mystr);
      RangeInCentimeters_f = RangeInCentimeters;
      Message msg(transceiver);  //  Will contain the structured sensor data.
      msg.addField("rng", RangeInCentimeters_f); 
      //  Send the message.
      if (msg.send()) {
        successCount++;  //  If successful, count the message sent successfully.
      } else {
        failCount++;  //  If failed, count the message that could not be sent.
      }
      delay(100000);  
}
